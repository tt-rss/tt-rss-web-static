# Archived Feed

Archived is the place for articles for which originating feed no longer exists.
It's either Starred articles from unsubscribed feeds or [externally shared
data](ShareAnything.md).

!!! notice

    Articles in Archived feed are not expired automatically, you can delete them manually
    using `Select...` &rarr; `Delete permanently` in the main toolbar.
